class Hl7Controller < ApplicationController

  def parse_hl7
    @hl7 = Hl7.new(message: parser_params[:message])

    respond_to do |format|
      format.js
    end
  end

  def hl7_parser
  end

  def hl7_lookup
    @dict = Hl7.field_dict.group_by{|k, v| k[0..2]}
  end

  def home
  end

  def adt_events
    @events = Hl7.adt_event_dict
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def parser_params
      params.permit(:message)
    end
end
