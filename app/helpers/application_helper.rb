module ApplicationHelper
    def title(page_title)
        output = "ParseHog - " + page_title
        content_for(:title) { output }
    end
end
