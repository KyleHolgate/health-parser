module Hl7DisplayHelper

    def field_tooltip(field)
        if field.index == 0
            output = field.seg_type + " : Segment Type"
        else
            output = field.seg_type + "-" + field.index.to_s + " : " + field.description
        end
        
        output
    end

    def uniq_seg_types
        Hl7.field_dict.group_by{|k, v| k[0..2]}
    end

end