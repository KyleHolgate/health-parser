class Segment
    attr_reader :body, :type, :order_key

    def initialize(body)
        @body = body
        @type = @body.split('|').first

        @fields = []
        @body.split("|", -1).each_with_index do |f, i|
            @fields << Field.new(value: f, seg_type: @type, index: i)
        end
    end

    def errors
        error_list = []

        if @type.size != 3
            error_list << "Invalid segment type '#{@type}'"
        elsif Hl7.field_dict[(@type+1.to_s).to_sym].nil?
            if @type.starts_with?('Z')
                error_list << "Unknown possible custom segment type '#{@type}'"
            else
                error_list << "Unknown segment type '#{@type}'"
            end
        end

        error_list
    end

    def field(i)
        #don't subtract 1 to account for 0 index because the first field
        #is the seg type which doesn't count towards field count
        fields[i]
    end

    def fields
        @fields
    end
end
