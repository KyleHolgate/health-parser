class Field
    attr_reader :seg_type, :index, :value

    def initialize(args={})
        @value = args[:value]
        @seg_type = args[:seg_type]
        @index = args[:index]

        #MSH fields need a +1 due to HL7 definitions considering
        #the first pipe to be a field itself
        if @seg_type == 'MSH'
            @index += 1
        end
    end

    def description
        description = Hl7.field_dict[(@seg_type + @index.to_s).to_sym]

        !description.nil? ? description : ""
    end

end