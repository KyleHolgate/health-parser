require 'forwardable'
class Segments
    include Enumerable
    extend Forwardable
    def_delegators :@segments, :each, :size, :[], :delete_if

    def initialize(message)
        @segments = []
        message.gsub('\n', "\n").each_line do |s|
            @segments << Segment.new(s.strip)
        end
    end

    def get_nth_segment(i)
        segments[i - 1]
    end

    def segment(type:, occurence: 1)
        segs = segments.select { |seg| seg.type == type }
        segs[occurence - 1]
    end

    private
    def segments
        @segments
    end
end