$(document).on('turbolinks:load', function() {
    $("body").tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $("#parser-interactive").on('click', 'button.crosswalk-toggle', function(){
        $(this).closest(".interactive-seg").next(".seg-crosswalk").slideToggle(200);
    });

    $("#message-select").change(function(){
        $("textarea#message").val($(this).val());
        $("textarea#message").trigger("focus").trigger("input");
    });

    $(document)
    .one('focus.auto-expand', 'textarea.auto-expand', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.auto-expand', 'textarea.auto-expand', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 22);
        this.rows = minRows + rows;
    });

    $( "#lookup-nav" ).change(function () {
        var str = "";
        $( "select option:selected" ).each(function() {
        str = $( this ).text();
        });
        console.log(str);
        $('html, body').animate({
            scrollTop: $("#"+str).offset().top - $('#lookup-nav').height()-10
        }, 400, 'linear');
    });

  });