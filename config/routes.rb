Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'hl7#home'

  get 'hl7_lookup' => 'hl7#hl7_lookup'
  get 'hl7_parser' => 'hl7#hl7_parser'
  get 'adt_events' => 'hl7#adt_events'
  post 'parse_hl7' => 'hl7#parse_hl7'
end
