require 'rails_helper'
include SampleHelper

describe Segments, type: :model do
  describe "Segments attributes" do
    let(:segments) { Segments.new(sample_a01) }

    it "splits message into array of segment" do
      expect(segments.size).to eq(4)
    end

    it "segment are type of segment" do
        #puts segments.inspect
        expect(segments.first).to be_a(Segment)
    end

    it "keeps order of segments passed in" do
        expect(segments[0].type).to eq('MSH')
        expect(segments[1].type).to eq('EVN')
        expect(segments[2].type).to eq('PID')
        expect(segments[3].type).to eq('PV1')
    end

  end

  describe "Segments methods" do
    let(:segments) { Segments.new(sample_discrete_lab) }

    it "getXthSegment accepts integer" do
        expect(segments.get_nth_segment(2).type).to eq('PID')
    end

    it "allows getSegment to accept a hash" do
        expect(segments.segment(type: 'OBX', occurence: 1).type).to eq('OBX')
    end

    it "defaults getSegment occurence to the first occurence" do
        expect(segments.segment(type: 'MSH').type).to eq('MSH')
    end

    it "correctly gets the nth occurence of a segment" do
        expect(segments.segment(type: 'OBX', occurence: 5).field(1).value).to eq('5')
    end

  end
end
