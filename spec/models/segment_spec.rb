require 'rails_helper'
include SampleHelper

describe Segment, type: :model do
  describe "basic segment functionality" do

    let(:segment) { Segment.new(sample_msh) }

    it "stores the segment text as body" do
      expect(segment.body).to eq(sample_msh)
    end

    it "sets the type on initialization" do
      #puts segments.inspect
      expect(segment.type).to eq('MSH')
    end

    it "stores fields as array of Field" do
      expect(segment.field(1)).to be_a(Field)
    end

    it "implements field() to return a field" do
      expect(segment.field(3).value).to eq('SENDING_FACILITY')
    end

    it "retains trailing empty fields" do
      nte_segment = Segment.new("NTE|||")
      expect(nte_segment.field(3).value).to eq("")
    end

    it "does not have errors on a valid segment" do
      expect(segment.errors.size).to eq(0)
    end

    it "adds error to the errors function for invalid segment type lengths" do
      invalid_seg = Segment.new("XX|Y|Z")
      expect(invalid_seg.errors.count).to eq(1)
      invalid_seg = Segment.new("XXXX|Y|Z")
      expect(invalid_seg.errors.count).to eq(1)
    end

    it "adds error to the errors function for unknown segment type" do
      invalid_seg = Segment.new("XYZ|Y|Z")
      expect(invalid_seg.errors.count).to eq(1)
    end

  end
end
