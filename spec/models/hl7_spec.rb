require 'rails_helper'
include SampleHelper

describe Hl7, type: :model do
  describe "hl7 attributes" do

    let(:hl7) { Hl7.new({message: sample_a01}) }

    it "body should not be nil" do
      expect(hl7.message).to_not be_nil
    end

    it "builds components on initialization" do
      expect(hl7.segments.size).to eq(4)
    end

    it "uses the Segments class" do
      expect(hl7.segments).to be_a(Segments)
    end

    it "does not have errors on a valid message" do
      expect(hl7.errors.size).to eq(0)
    end

    it "adds error to the errors function for missing PID/MSH segment" do
      missing_MSH = Hl7.new({message: sample_a01})
      missing_MSH.segments.delete_if{|seg| seg.type == 'MSH'}
      expect(missing_MSH.errors.count).to eq(1)

      missing_PID = Hl7.new({message: sample_a01})
      missing_PID.segments.delete_if{|seg| seg.type == 'PID'}
      expect(missing_PID.errors.count).to eq(1)
    end

    it "should have a field_dict method that returns a hash" do
      expect(Hl7.field_dict).to be_a(Hash)
    end

    it "should have a adt_event_dict method that returns a hash" do
      expect(Hl7.adt_event_dict).to be_a(Hash)
    end
  end
end
