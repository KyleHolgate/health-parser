require 'rails_helper'

describe Field, type: :model do
  describe "basic Field functionality" do

    let(:field) { Field.new(value: 'Detected', seg_type: 'OBX', index: 5) }

    it "accesses the value" do
        expect(field.value).to eq('Detected')
    end

    it "accesses the seg_type" do
        expect(field.seg_type).to eq('OBX')
    end

    it "accesses the index" do
        expect(field.index).to eq(5)
    end

    it "looks up correct description" do
        expect(field.description).to eq('Observation Value')
    end

  end
end
