require 'rails_helper'

describe "hl7/_hl7_parser.html.haml" do
    it "should display the form with the correct action" do
        render
        expect(rendered).to match(/form action="\/parse_hl7"/i)
    end
end