require "rails_helper"

RSpec.describe Hl7Controller, type: :routing do
  describe "routing" do
    it "routes to #home" do
      expect(:get => "/").to route_to("hl7#home")
    end
  end
end
