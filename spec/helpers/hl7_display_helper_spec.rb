require 'rails_helper'
include SampleHelper

# Specs in this file have access to a helper object that includes
# the ParsersHelper. For example:
#
 describe Hl7DisplayHelper do
   describe "utility functions" do
    let(:segment) { Segment.new(sample_msh) }

        it "handles MSH descriptions by skipping an index when outputting display" do
            expect(helper.field_tooltip(segment.field(3))).to match(/MSH-4.*Sending Fac/i)
        end
    end
 end